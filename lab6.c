#include<stdio.h>
int main()
{
	int matrix[3][3];
	int tmatrix[3][3];
	int i,j;
	for(i=0;i<3;i++)
	{
		printf("Enter the elements of row %d=  ",i+1);
		for(j=0;j<3;j++)
		{
			scanf("%d",&matrix[i][j]);
		}
	}
	printf("The matrix you have entered is: \n\n");
	for(i=0;i<3;i++)
	{
		printf("| ");
		for(j=0;j<3;j++)
		{
			printf("%d ",matrix[i][j]);
		}
		printf(" |\n");
	}
	for(i=0;i<3;i++)
	{
		for(j=0;j<3;j++)
		{
			tmatrix[j][i]=matrix[i][j];
		}
	}
	printf("The transpose of the matrix is :\n\n");
	for(i=0;i<3;i++)
	{
		printf("| ");
		for(j=0;j<3;j++)
		{
			printf("%d ",tmatrix[i][j]);
		}
		printf(" |\n");
	}
return 0;
}